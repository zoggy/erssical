(******************************************************************************)
(*               Erssical                                                     *)
(*                                                                            *)
(*   Copyright (C) 2013-2021 Institut National de Recherche en Informatique   *)
(*   et en Automatique. All rights reserved.                                  *)
(*                                                                            *)
(*   This program is free software; you can redistribute it and/or modify     *)
(*   it under the terms of the GNU Lesser General Public License version      *)
(*   3 as published by the Free Software Foundation.                          *)
(*                                                                            *)
(*   This program is distributed in the hope that it will be useful,          *)
(*   but WITHOUT ANY WARRANTY; without even the implied warranty of           *)
(*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *)
(*   GNU Library General Public License for more details.                     *)
(*                                                                            *)
(*   You should have received a copy of the GNU Library General Public        *)
(*   License along with this program; if not, write to the Free Software      *)
(*   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                 *)
(*   02111-1307  USA                                                          *)
(*                                                                            *)
(*   Contact: Maxence.Guesdon@inria.fr                                        *)
(*                                                                            *)
(*                                                                            *)
(******************************************************************************)

(** *)

open Rss;;
open Types;;
module XR = Xtmpl.Rewrite

let map_opt f = function None -> "" | Some x -> f x;;
let s_opt = function None -> "" | Some s -> s;;

let rec first_tree = function
  [x] -> x
| [] -> XR.cdata ""
| XR.D _ :: x -> first_tree x
| t :: _ -> t

let tree_opt = function
  None -> XR.cdata ""
| Some s ->
  try first_tree (XR.from_string s)
  with _ ->
    XR.cdata s
;;

let on_source_opt f = function
  None -> XR.cdata ""
| Some s -> XR.cdata (f s)
;;

let on_data_opt f = function
  None -> XR.cdata ""
| Some d -> f d
;;

let pyear d = let (y,_,_) = Ptime.to_date d in y
let pmonth d = let (_,m,_) = Ptime.to_date d in m
let pday d = let (_,_,day) = Ptime.to_date d in day
let phour d = let _, ((h,_,_), _) = Ptime.to_date_time d in h
let pmin d = let _, ((_,m,_), _) = Ptime.to_date_time d in m
let psec d = let _, ((_,_,s), _) = Ptime.to_date_time d in s

let month_of_int = function
| 1 -> "Jan"
| 2 -> "Feb"
| 3 -> "Mar"
| 4 -> "Apr"
| 5 -> "May"
| 6 -> "Jun"
| 7 -> "Jul"
| 8 -> "Aug"
| 9 -> "Sep"
| 10 -> "Oct"
| 11 -> "Nov"
| 12 -> "Dec"
| n -> failwith (Printf.sprintf "Invalid month %d" n)

let string_of_date d =
  let (y, m, day) = Ptime.to_date d in
  Printf.sprintf "%d %s %04d" day (month_of_int m) y

let env_of_item item =
  let mk env (tag, xml) = XR.env_add_cb tag (fun x _ ?loc _ _ -> (x, [xml])) env in
  List.fold_left mk (XR.env_empty ())
    [
      "item-title", XR.cdata (s_opt item.item_title);
      "item-description", tree_opt item.item_desc ;
      "item-url", XR.cdata (map_opt Types.string_of_url item.item_link) ;
      "item-pubdate", XR.cdata (map_opt string_of_date item.item_pubdate) ;
      "item-pub-year", XR.cdata (map_opt (fun d -> string_of_int (pyear d)) item.item_pubdate) ;
      "item-pub-month", XR.cdata (map_opt (fun d -> Printf.sprintf "%02d" (pmonth d)) item.item_pubdate) ;
      "item-pub-mday", XR.cdata (map_opt (fun d -> Printf.sprintf "%02d" (pday d)) item.item_pubdate) ;
      "item-pub-hour", XR.cdata (map_opt (fun d -> Printf.sprintf "%02d" (phour d)) item.item_pubdate) ;
      "item-pub-minute", XR.cdata (map_opt (fun d -> Printf.sprintf "%02d" (pmin d)) item.item_pubdate) ;
      "item-pub-second", XR.cdata (map_opt (fun d -> Printf.sprintf "%02d" (psec d)) item.item_pubdate) ;
      "item-author", XR.cdata (s_opt item.item_author) ;
      "item-source-url", on_source_opt (fun s -> Types.string_of_url s.src_url) item.item_source ;
      "item-source-name", on_source_opt (fun s -> s.src_name) item.item_source ;
      "item-ev-keywords", on_data_opt (fun d -> XR.cdata (String.concat ", " d.ev_keywords)) item.item_data ;
    ]

let apply_template tmpl channel =
  match tmpl with
    XR.E {XR.name ;atts ; subs = item_tmpl} ->
      let f item =
        let env = env_of_item item in
        snd (XR.apply_to_xmls () env item_tmpl)
      in
      let xmls = List.map f channel.ch_items in
      XR.node name ~atts (List.flatten xmls)
  | XR.D _
  | XR.C _
  | XR.PI _ -> assert false (* FIXME: use a better exception, with a message *)
;;