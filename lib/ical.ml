(******************************************************************************)
(*               Erssical                                                     *)
(*                                                                            *)
(*   Copyright (C) 2013-2021 Institut National de Recherche en Informatique   *)
(*   et en Automatique. All rights reserved.                                  *)
(*                                                                            *)
(*   This program is free software; you can redistribute it and/or modify     *)
(*   it under the terms of the GNU Lesser General Public License version      *)
(*   3 as published by the Free Software Foundation.                          *)
(*                                                                            *)
(*   This program is distributed in the hope that it will be useful,          *)
(*   but WITHOUT ANY WARRANTY; without even the implied warranty of           *)
(*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *)
(*   GNU Library General Public License for more details.                     *)
(*                                                                            *)
(*   You should have received a copy of the GNU Library General Public        *)
(*   License along with this program; if not, write to the Free Software      *)
(*   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                 *)
(*   02111-1307  USA                                                          *)
(*                                                                            *)
(*   Contact: Maxence.Guesdon@inria.fr                                        *)
(*                                                                            *)
(*                                                                            *)
(******************************************************************************)

(** *)

open Rss
open Types

let ical_link_of_item item =
  match item.Rss.item_data with
    Some { ev_link = Some url } -> Some url
  | _ ->
  match item.item_link with
    None -> None
  | Some url -> Some url
;;

module USet = Set.Make
  (struct type t = Uri.t let compare = Types.compare_url end)

let remove_duplicated_events items =
  let rec iter acc seen = function
    [] -> List.rev acc
  | item :: q ->
    match ical_link_of_item item with
      None -> iter (item :: acc) seen q
    | Some url ->
      if USet.mem url seen then
        iter acc seen q
      else
        iter (item :: acc) (USet.add url seen) q
  in
  iter [] USet.empty items
;;

open Icalendar

let component_of_item i =
  let dtstart, dtend =
    let (dtstart, dtend) =
      match i.Rss.item_data with
        None -> (None, None)
      | Some ev -> (ev.ev_start, ev.ev_end)
    in
    match dtstart with
      None ->
        begin
          match i.item_pubdate with
            None -> (None, None)
          | Some dt -> (Some dt, None)
        end
    | _ -> (dtstart, dtend)
  in
  let s_opt = function None -> "" | Some s -> s in
  match dtstart with
  | None -> None
  | Some dtstart ->
      let props =
        [
          `Transparency (Params.empty, `Transparent) ;
          `Summary (Params.empty, s_opt i.item_title) ;
          `Description (Params.empty, s_opt i.item_desc) ;
        ]
      in
      let props =
        match ical_link_of_item i with
        | None -> props
        | Some url -> (`Url (Params.empty, url)) :: props
      in
      let props =
        match i.item_data with
        | None -> props
        | Some ev ->
            let props =
              match ev.ev_location with
              | None -> props
              | Some loc ->
                  let params = Params.empty in
                  let params =
                    match loc.loc_href with
                    | None -> params
                    | Some url -> Params.add Altrep url params
                  in
                  `Location (params, loc.loc_name) :: props
            in
            let cats = `Categories (Params.empty, ev.ev_keywords) in
            cats :: props
      in
      let uid = match i.item_guid with
        | None -> ""
        | Some (Guid_permalink url) -> Types.string_of_url url
        | Some (Guid_name s) -> s
      in
      let dtstamp = match i.item_pubdate with
        | Some d -> d
        | None -> match Ptime.of_float_s 0. with
            | None -> failwith "Invalid time ???"
            | Some d -> d
      in
      let t =
        { dtstamp = (Params.empty, dtstamp) ;
          uid = (Params.empty, uid) ;
          dtstart = (Params.empty, `Datetime (`Utc dtstart)) ;
          dtend_or_duration = (
           match dtend with
           | None -> None
           | Some d -> Some (`Dtend (Params.empty, `Datetime (`Utc d)))) ;
          rrule = None ;
          props ;
          alarms = [] ;
        }
      in
      Some (`Event t)

let ical_of_channel ch =
  let props = [
      `Version (Params.empty, "2.0") ;
      `Prodid (Params.empty, "-//erssical/1.0") ;
    ]
  in
  let items = remove_duplicated_events ch.ch_items in
  let components = List.fold_left
    (fun acc item ->
       match component_of_item item with
       | None -> acc
       | Some c -> c :: acc)
      [] items
  in
  let cal = (props, List.rev components) in
  Fmt.to_to_string Icalendar.pp cal
